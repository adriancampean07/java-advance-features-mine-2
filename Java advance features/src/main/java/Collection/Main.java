package Collection;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<String> visitedCountries = new ArrayList<>();
        visitedCountries.add("Germany");
        visitedCountries.add("France");
        visitedCountries.add("Spain");
        visitedCountries.remove("France");

        for (String country : visitedCountries) {
            System.out.println(country + " ");
        }

        System.out.println("---------");

        System.out.println(visitedCountries.size());

        System.out.println("---------");

        List<String> countries = new ArrayList<>();
        countries.add("Italy");
        countries.add("Romania");
        countries.add("Spain");
        countries.add("Russian");

        visitedCountries.addAll(countries);


        System.out.println("---------");

        Iterator<String> iterator2 = countries.iterator();
        while (iterator2.hasNext()) {
            System.out.println(iterator2.next());
        }

        System.out.println("---------");

        for (String country : visitedCountries) {
            if (country.startsWith("R")) {
                System.out.println(country);

            }
        }

        System.out.println("---------");

        for (String country : visitedCountries) {
            if (country.contains("r") || country.contains("R")) {
                System.out.println(country);
            }

        }

        System.out.println("-------------");

        Iterator<String> iterator = visitedCountries.iterator();
        while (iterator.hasNext()) {
            //    System.out.println(iterator.next());
            String country = iterator.next();
            if (country.contains("m")) {
                System.out.println(country);
            }
        }

        System.out.println("-------------");

        visitedCountries.add(3,"Patagonia");
        for(String country: visitedCountries){
            System.out.print(country + " ");
        }
        System.out.println(visitedCountries.size());

        for (int i = 0; i < visitedCountries.size()-1; i++){
            if(visitedCountries.get(i+1).startsWith("R")){
                System.out.println(visitedCountries.get(i));
            }
        }

        System.out.println();
        visitedCountries.remove(2);
        for(String country: visitedCountries){
            System.out.print(country + " ");
        }

        System.out.println();

        visitedCountries.remove("Patagonia");
        for(String country: visitedCountries) {
            System.out.print(country + " ");
        }

        System.out.println();

        visitedCountries.add("Romania");
        for(String country: visitedCountries) {
            System.out.print(country + " ");
        }


        System.out.println("\n-------");

        Set<String> colours = new HashSet<>();
        colours.add("black");
        colours.add("blue");
        colours.add("orange");
        colours.add("red");
        colours.add("green");

        for(String colour :colours ){
            System.out.print(colour + " ");
        }

        colours.remove("orange");
        for(String colour:colours){
            System.out.print(colour + " ");
        }

        colours.add("purple");
        for(String colour:colours){
            System.out.print(colour + " ");
        }

        System.out.println("\n-------");

        Set<String> colours2 = new TreeSet<>();
        colours2.add("black");
        colours2.add("blue");
        colours2.add("orange");
        colours2.add("red");
        colours2.add("green");

        for(String colour :colours2 ){
            System.out.print(colour + " ");
        }




    }
}
