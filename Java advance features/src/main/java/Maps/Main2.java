package Maps;

import java.util.HashMap;
import java.util.Map;

public class Main2 {
    public static void main(String[] args) {
        Map<String,Integer> students = new HashMap<>();

        students.put("George",8);
        students.put("Paul",7);
        students.put("Ioana",4);
        students.put("Laura",10);
        students.put("Andrei",5);

        for(Map.Entry<String,Integer> student: students.entrySet()){
            System.out.println(student);
            String key = student.getKey();
            Integer value = student.getValue();
            System.out.printf("%s : %d  \n", key,value );
        }

        for(Integer nota : students.values()){
            System.out.println(nota);
        }

        System.out.println("Nota Ioanei" + ":" + " " + students.get("Ioana"));
    }
}
