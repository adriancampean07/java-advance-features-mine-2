package exercitii;

public class Staff extends Person {
    protected String specialization;
    protected int salary;

    @Override
    public void f() {
        System.out.println("Sunt angajatul " + name);
    }

    public Staff(String name, String address, String specialization, int salary) {
        super(name,address);
        this.specialization = specialization;
        this.salary = salary;
    }

    public Staff(String specialization, int salary) {
        super();
        this.specialization = specialization;
        this.salary = salary;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Staff{" +
                "specialization='" + specialization + '\'' +
                ", salary=" + salary +
                ", name='" + name + '\'' +
                ", adress='" + address + '\'' +
                '}';
    }
}
