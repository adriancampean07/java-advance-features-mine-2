import solid.Car;
import solid.ElectricCar;
import solid.MotorCar;

public class Main {
    public static Car car;

    public static void drive(Car car){
        car.Start();
        car.accelerate( 100);
        car.stop();

    }

    public static void specificDrive(ElectricCar electricCar){
        electricCar.Start();
        electricCar.accelerate(150);
        electricCar.stop();
        electricCar.regeneratingBreaking();

    }

    public static void main(String[] args) {
        car = new MotorCar();
        drive(car);

        System.out.println("--------");

        Car electricCar = new ElectricCar();
        drive(electricCar);

        System.out.println("---------");

        ElectricCar carSpecific = new ElectricCar();
        specificDrive(carSpecific);
        drive(carSpecific);
    }
}
