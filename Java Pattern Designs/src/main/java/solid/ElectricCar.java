package solid;

public class ElectricCar implements Car {
    private Engine engine;

    public void regeneratingBreaking(){
        engine.Stop();
        System.out.println("Regenerating breaking");
    }

    public ElectricCar() {
        this.engine = new Engine();
    }

    public void Start() {
        System.out.println("Tesla");
        engine.Run();
    }

    public void accelerate(int kph) {
        System.out.println("Start moving");
        engine.Speed(kph);

    }

    public void stop() {
        System.out.println("Cas has stopped");
    engine.Stop();
    }


}
