package com.company;

public class Home {
    String name;
    public Home(){
    }
    public static void main(String arg[]){
        Mouse m1 = new Mouse();
        m1.name = "Jerry";
        Mouse m2 = new Mouse();
        m2.name = "Nibbles";
        Mouse m3 = new Mouse();
        m3.name = "Mickey";
        m1 = m3;
        System.out.println(m1.name);
        System.out.println(m2.name);
        System.out.println(m3.name);

    }
}
