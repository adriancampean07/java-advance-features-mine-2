package com.company;

public class Car {
    private int doors;
    private int wheels;
    private String color;
    private String model;
    private int km = 0;
    private  int accidente = 0;

    public void registerAccidente(){
        this.accidente ++;
    }

    public void registerKM(){
        this.km ++;
    }
    public void setDoors(int doors){
        this.doors = doors;
    }
    public void setWheels(int wheels){
        this.wheels = wheels;
    }
    public int getWheels(){
        return this.wheels;
    }
    public int getKm(){
        return this.km;
    }
    public int getAccidente(){
        return  this.accidente;
    }
}