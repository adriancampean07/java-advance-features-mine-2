package com.company;

public class Main {


    public static void main(String[] args) {
    toMilesPerHour( 1.5);
    toMilesPerHour(-5.6);
    toMilesPerHour(10.25);
    toMilesPerHour(25.42);
    toMilesPerHour(75.114);
    toMilesPerHour(2.5);
    }
    public static void toMilesPerHour ( double kilometerPerHour)
    {   if (kilometerPerHour < 0) {
        System.out.println("invalid value");
        return;
    }
        long round = Math.round(kilometerPerHour/1.609);
        System.out.println(kilometerPerHour + "km/h="  +" "+ round +" " + "mi/h");

    }
}
