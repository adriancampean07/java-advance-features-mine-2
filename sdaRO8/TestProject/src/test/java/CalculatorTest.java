import FirstPackege.Calculator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Random;

public class CalculatorTest {
    @BeforeClass
    public static void setUp(){
        System.out.println("Message before test");
    }
    @AfterClass
    public static void  tearDown(){
        System.out.println("Message after test");
    }
    @Test(expected = ArithmeticException.class)
    public void shouldThrowExceptionWhenDividingBy0() {
        System.out.println("Message test1");
        // given
        Calculator calculator = new Calculator();
        int number = new Random().nextInt();
        // when
        calculator.divide(number, 0);
        // then
        // should throw exception
    }
    @Test(expected = java.lang.AssertionError.class)
    public void test() {
        System.out.println("Message test2");
        // given
        Calculator calculator = new Calculator();
        // when
        int result = calculator.add(5, 3);
        // then
        assert result == 7;
    }
}
