package com.company;

public class Main {

    public static void main(String[] args) {

        System.out.println(isPalindrome(-1221)); //→ should return true
        System.out.println(isPalindrome(707)); //→ should return true
        System.out.println(isPalindrome(11212)); //→ should return false because reverse is 21211 and that is not equal to 11212.



    }
    public static boolean isPalindrome(int number) {

        int original = number;
        int revers = 0;

        while (number != 0) {


            int last = number % 10;
            number = number /10;
            revers = revers * 10;
            revers = revers + last;

        }
        if (original == revers) {
            return true;
        } else
            return false;
    }
}
