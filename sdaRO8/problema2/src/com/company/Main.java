package com.company;

public class Main {

    public static void main(String[] args) {
        Wall wall1 = new Wall(3,6);
        System.out.println(wall1.getArea());

        Wall wall2 = new Wall();
        wall2.setWidth(3.6);
        wall2.setHeight(5.9);
        System.out.println(wall2.getArea());
    }
}
