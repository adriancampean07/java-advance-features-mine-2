package com.company;

public class BankAccount {
    private int accountNumber;
    private double balance;
    private String costumerName;
    private String email;
    private String phone;

    public BankAccount(int accountNumber, String customerName, String email, String phone){
        this.accountNumber = accountNumber;
        this.balance = 0;
        this.costumerName = customerName;
        this.email = email;
        this.phone = phone;

    }

    public void deposit(double amount){
        this.balance = this.balance + amount;
    }

    public void withdraw(double amount){
        if (amount > balance){
            System.out.println("Insufficient funds");
            return;
        }
        this.balance = this.balance - amount;
    }


    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCostumerName() {
        return costumerName;
    }

    public void setCostumerName(String costumerName) {
        this.costumerName = costumerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
