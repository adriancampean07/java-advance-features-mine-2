package com.company;

public class BankAccount {
    private double accountnumber;
    private double balance;
    private String costumerName;
    private String email;
    private int phoneNumber;

    public void deposit( double deposit){
        this.balance = this.balance + deposit;
    }
    public void withdrawl(double withdraw) {
        this.balance = this.balance - withdraw;
        if (withdraw > balance) {
            System.out.println("Not Allow");
        }
        return;
    }


    public double getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(double accountnumber) {
        this.accountnumber = accountnumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCostumerName() {
        return costumerName;
    }

    public void setCostumerName(String costumerName) {
        this.costumerName = costumerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
