package com.company;

public class Main {

    public static void main(String[] args) {
        printMegaBytesAndKiloBytes(2500);
        printMegaBytesAndKiloBytes(-1024);
        printMegaBytesAndKiloBytes(5);
        printMegaBytesAndKiloBytes(5000);

    }

    public static void printMegaBytesAndKiloBytes (int kiloBytes){
        if(kiloBytes <=0){
            System.out.println("invalid Value");
            return;
        }
        int yy= kiloBytes / 1024;
        int zz = kiloBytes % 1024;

        System.out.println(kiloBytes + " KB =" + yy + " MB and " + zz + " KB");
    }
}
