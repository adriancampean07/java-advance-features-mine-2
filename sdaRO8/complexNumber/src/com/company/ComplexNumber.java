package com.company;

public class ComplexNumber {
    private double real;
    private double imaginary;

    public ComplexNumber(double real, double imaginary){
        this.real = real;
        this.imaginary = imaginary;
    }

    public void add(double real, double imaginary){
        this.real = this.real + real;
        this.imaginary = this.imaginary + imaginary;
    }

    public void add (ComplexNumber cnumber){
//        this.real = cnumber.getReal()+ this.real;
//        this.imaginary = cnumber.getImaginary() + this.imaginary;
        add(cnumber.getReal(), cnumber.getImaginary());
    }

    public void subtract (double real, double imaginary){
        this.real -=real;
        this.imaginary -=imaginary;
    }

    public void subtract(ComplexNumber cnumber){
        subtract(cnumber.getReal(), cnumber.getImaginary());
    }

    public double getReal() {
        return real;
    }

    public double getImaginary() {
        return imaginary;
    }
}
