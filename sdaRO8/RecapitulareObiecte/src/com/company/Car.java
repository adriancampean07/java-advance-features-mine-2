package com.company;

public class Car {
    private String color;
    private String model;
    private int km;
    private int door;


    public static double calculate(double price, double wheels){
        return price * wheels;

    }

    public Car(String color, String model, int door) {
        this.color = color;
        this.model = model;
        this.km = 0;
        this.door = door;
    }
    public Car(){}

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getDoor() {
        return door;
    }

    public void setDoor(int door) {
        this.door = door;
    }
}
