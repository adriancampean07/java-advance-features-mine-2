package Interfaces;

public class Main {
    public static void main(String[] args) {
        Hunter hunter = new Hunter();
        Soldier soldier = new Soldier();
        hunter.speak("");
        System.out.println(soldier.stillBreathing());
        System.out.println(hunter.stillBreathing());
    }
    //public static (Human soldier)
}
