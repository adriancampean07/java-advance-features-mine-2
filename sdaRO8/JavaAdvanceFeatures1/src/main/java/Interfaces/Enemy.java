package Interfaces;

public interface Enemy {
    public void speak(String statement);
    public boolean moveTo(int x, int y);
    public int attack();
    public void heal(int amt);
//    public void eventOnDeath();
}
