package Interfaces;

public class Soldier implements Human{
    private int life;

    public Soldier (int life){
        this.life = life;
    }
    public Soldier(){
        this.life = 30;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    @Override
    public boolean stillBreathing() {
        if(life > 0){
            return true;
        }
        return false;
    }

    @Override
    public int fight() {
        return 2;

    }

    @Override
    public String toString() {
        return "Soldier{" +
                "life=" + life +
                '}';
    }
}
