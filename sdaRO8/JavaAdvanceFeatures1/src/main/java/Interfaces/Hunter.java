package Interfaces;

public class Hunter implements Human, Enemy {

    private final String STATEMENT = "I'm the hunter";
    private int health;

    public Hunter(int health) {
        this.health = health;
    }

    public Hunter() {
        this.health = 20;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    @Override
    public void speak(String statement) {
        if (statement == null || statement.isEmpty()) {
            System.out.println(STATEMENT);
        } else {
            System.out.println(statement);
        }
    }

    @Override
    public boolean moveTo(int x, int y) {
        if (x + y == 7) {
            return false;
        }
        return true;
    }

    @Override
    public int attack() {
        System.out.println(STATEMENT + " I'm attacking");
        return 5;

    }

    @Override
    public void heal(int amt) {
        this.health = health + amt;
        System.out.println("I have healed");
    }

    @Override
    public boolean stillBreathing() {
        System.out.println("Hunter still breathing");
        if(health > 0){
            return true;
        }
        return false;

    }

    @Override
    public int fight() {
        System.out.println("I'm fighting");
        return 3;

    }

    @Override
    public String toString() {
        return "Hunter{" +
                ", health=" + health +
                '}';
    }
}
