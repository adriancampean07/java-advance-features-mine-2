package Inheritance;

import java.util.List;

public class Shape {
    private Integer edgeNumber;
    private String name;
    private List<Integer> edges;

    public Shape(Integer edgeNumber, String name, List<Integer> edges) {
        this.edgeNumber = edgeNumber;
        this.name = name;
        this.edges = edges;
  //      this.perimeter = calculatePerimeter();
    }

    public Shape() {
        //empty
    }

    public Integer getEdgeNumber() {
        return edgeNumber;
    }

    public void setEdgeNumber(Integer edgeNumber) {
        this.edgeNumber = edgeNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getEdges() {
        return edges;
    }

    public void setEdges(List<Integer> edges) {
        this.edges = edges;
    }



    @Override
    public String toString() {
        return "[toString] Parent class Shape{" +
                "edgeNumber=" + edgeNumber +
                ", name='" + name + '\'' +
                ", edges=" + edges +
                '}';
    }
}
