package Inheritance;

import java.util.List;

public class Cicle extends Shape {
    private static final float PI = 3.14f;
    private Integer area;
    private Integer radius;
    private Integer perimeter;

    public Cicle(Integer edgeNumber, String name, List<Integer> edges, Integer radius) {
        super(edgeNumber, name, edges);
        this.radius = radius;
        this.perimeter = calculatePerimeter();
        this.area = calculateArea();
    }
    public Integer calculateArea(){
        return Integer.valueOf("" + ((int) PI) * radius * radius);
    }


    public Integer calculatePerimeter(){
        return Integer.valueOf( "" + 2 * ((int) PI) * this.radius);
    }

//    @Override
//    public String toString() {
//        return super.toString() + "[toString] Child class Cicle{" +
//                "area=" + area +
//                ", perimeter=" + perimeter +
//                ", radius=" + radius +
//                '}';
//    }
}
