package Compostion;

public class Muzzle {
    private boolean needMuzzle;
    private String material;

    public Muzzle(boolean needMuzzle, String material){
        this.needMuzzle = needMuzzle;
        this.material = material;
    }
    public Muzzle(){
        //empty
    }

    public boolean getNeedMuzzle() {
        return this.needMuzzle;
    }

    public void setNeedMuzzle(boolean needMuzzle) {
        this.needMuzzle = needMuzzle;
    }

    public String getMaterial() {
        return this.material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Muzzle{" +
                "needMuzzle=" + needMuzzle +
                ", material='" + material + '\'' +
                '}';
    }
}
