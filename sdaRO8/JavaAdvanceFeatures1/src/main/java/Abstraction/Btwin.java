package Abstraction;

    public class Btwin extends Bike {
        private static final String Btwin = "Btwin";
        public Btwin(){
            //empty
        }
        public  Btwin(int speed){
            super.speed = speed;
        }
        @Override
        public void run(){
            System.out.println("I'm Btwin and i'm running safely");

        }

        @Override
        public String toString() {
            return "Btwin{" +
                    "speed=" + speed +
                    '}';
        }
    }

