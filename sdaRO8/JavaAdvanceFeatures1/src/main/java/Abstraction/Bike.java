package Abstraction;

public abstract class Bike {
    protected int speed;

    public abstract void run();

    public void sayHi(){
        System.out.println("Hello");
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
