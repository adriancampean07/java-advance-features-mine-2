package Abstraction;

public class Pegasus extends Bike {
    private static final String PEGASUS = "PEGASUS";
    public Pegasus (){
        //empty
    }
    public  Pegasus(int speed){
        super.speed = speed;
    }
    @Override
    public void run(){
        System.out.println("I'm Pegasus and i'm running safely");

    }

    @Override
    public String toString() {
        return "Pegasus{" +
                "speed=" + speed +
                '}';
    }
}
