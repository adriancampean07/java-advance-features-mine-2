package Abstraction;

public class Main {
    public static void main(String[] args) {
        Pegasus bik1 = new Pegasus(20);
        Btwin bike2 = new Btwin(30);
        System.out.println(bik1);
        System.out.println(bike2);
        Bike bike3 = new Pegasus(25);
        Bike bike4 = new Btwin(35);
        System.out.println(bike3);
        System.out.println(bike4);
        bike4.sayHi();
        bike3.run();
    }
}

