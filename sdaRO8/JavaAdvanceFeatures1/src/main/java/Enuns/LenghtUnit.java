package Enuns;

public enum LenghtUnit {
    METER(1, "metru"),
    FOOT(0.3, "foot"),
    INCH(0.025, "inch"),
    CENTIMETER(0.01, "cm"),
    MILIMETER(0., "milimeter");

    double value;

    String prettyName;

    LenghtUnit(double value, String prettyName) {
        this.value = value;
        this.prettyName = prettyName;
    }

    public double convertToMeters() {
        return value;
    }
    public double f(double val){
        return val * value;
    }

    @Override
    public String toString() {
        return prettyName;
    }
}
