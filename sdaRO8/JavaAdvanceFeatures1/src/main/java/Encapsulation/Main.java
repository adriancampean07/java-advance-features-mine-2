package Encapsulation;

import Inheritance.Cicle;
import Inheritance.Shape;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        Dog dog1=new Dog();
//        Dog dog2=new Dog("female","dalmatian");
//        Dog dog3=new Dog("Rex",5,"Male",20,"ChowChow");
//        Dog dog4 = new Dog("grivei");
//        Dog dog5 = new Dog("Azorel",7);
//        Dog dog6 = new Dog();
//        Object object = new Object();
//        System.out.println(dog1.toString());
//        System.out.println(dog2.toString());
//        System.out.println(dog3.toString());
//        System.out.println(dog4.toString());
//        System.out.println(dog5.toString());
//
//        Cat cat1 = new Cat();
//        System.out.println(cat1.toString());
//        cat1.setPaws(4);
//        cat1.setName("Maw");
//        System.out.println(cat1.toString());
//        Cat cat2 = new Cat(3,"Wiskey");
//        System.out.println(cat2.toString());

        List<Integer> edges = new ArrayList<Integer>();
        edges.add(2);
        edges.add(5);
        edges.add(3);
        edges.add(4);
        Shape shape = new Shape(4,"Irregular",edges);
        System.out.println(shape.toString());
        edges.clear();
        edges.add(5);
        Cicle circle1 = new Cicle(1,"myCircle", edges,5);
        System.out.println(circle1.toString());
        Shape circle2 = new Cicle(4,"myCircle2",edges,3);
        System.out.println(circle2.toString());
        printEdgesNumber(circle1);
        printWithPrefix("MyCircle",circle1);

        //Muzzle muzzle = new Muzzle(true,"leather"); // acelas lucru l-am facut mai jos
//        Dog dogC = new Dog("Rex",5,"Male",20,"ChowChow",new Muzzle(true,"leather"));
//        System.out.println(dogC);
//        System.out.println(dogC.getMuzzle().getNeedMuzzle());
//        System.out.println(dogC.getMuzzle().getMaterial());
//        System.out.println(dogC.getMuzzle());
//        System.out.println(dogC.getMuzzle().hashCode());


//        LenghtUnit meterUnit = LenghtUnit.METER;
//        System.out.println(meterUnit);
//        System.out.println(LenghtUnit.METER.convertToMeters());
//        double val = LenghtUnit.CENTIMETER.convertToMeters();
//        System.out.println(5 * val);
//        System.out.println(LenghtUnit.CENTIMETER.f(5));

//        Pegasus bik1 = new Pegasus(20);
//        Btwin bike2 = new Btwin(30);
//        System.out.println(bik1);
//        System.out.println(bike2);
//        Bike bike3 = new Pegasus(25);
//        Bike bike4 = new Btwin(35);
//        System.out.println(bike3);
//        System.out.println(bike4);
//        bike4.sayHi();
//        bike3.run();
    }

    public static void printEdgesNumber(Shape shape){
        System.out.println(shape.getEdgeNumber());
    }
    public static void printWithPrefix(String prefix,Object object){
        System.out.println(prefix + object.toString());
    }

}

