package Encapsulation;

import Compostion.Muzzle;

public class Dog {
    private String name;
    private int age;
    private String gender;
    private int weight;
    private String race;
    private Muzzle muzzle;

    public Dog(String name, int age, String gender, int weight, String race, Muzzle muzzle) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.weight = weight;
        this.race = race;
        this.muzzle = muzzle;
    }

    public Dog(String name, int age, String gender, int weight, String race) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.weight = weight;
        this.race=race;
    }
    public Dog (String gender, String race){
        this();
        this.gender=gender;
        this.race=race;
//      new Dog();
    }
    public Dog(){
        this.name="Bobi";
        this.age=1;
        this.weight=10;
        this.gender="neutral";
        this.race="undefined";
        this.muzzle = new Muzzle(false,"undefined");
    }
    public Dog(String name){
        this();
        this.name=name;
    }
    public Dog(String name,int age){
        this(name);
        this.age=age;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        if(gender!= null && !"".equals(gender))
        this.gender = gender;
    }
    public int getWeight() {
        return weight;
    }
    public void setWeight(int weight) {
        this.weight = weight;
    }
    public String getRace() {
        return race;
    }
    public void setRace(String race) {
        this.race = race;
    }

    public Muzzle getMuzzle() {
        return muzzle;
    }

    public void setMuzzle(Muzzle muzzle) {
        this.muzzle = muzzle;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", weight=" + weight +
                ", race='" + race + '\'' +
                ", muzzle=" + muzzle+
                '}';
    }
}
