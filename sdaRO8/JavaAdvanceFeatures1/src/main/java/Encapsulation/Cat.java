package Encapsulation;

public class Cat {
    private int paws;
    private String name;

    public Cat (int paws, String name){
        this.paws = paws;
        this.name = name;
    }
    public Cat(){
    }

    @Override
    public String toString() {
        return "Cat{" +
                "paws=" + paws +
                ", name='" + name + '\'' +
                '}';
    }

    public int getPaws() {
        return paws;
    }

    public void setPaws(int paws) {
        this.paws = paws;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
