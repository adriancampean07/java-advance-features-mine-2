package com.company;

public class Main {

    public static void main(String[] args) {
        SimpleCalculator c = new SimpleCalculator();
        c.setFirstNumber(5);
        c.setSecondNumber(7);

        System.out.println(c.getFirstNumber());
        System.out.println(c.getSecondNumber());

        double result = c.getAdditionResult();
        System.out.println(result);

        SimpleCalculator  c2 = new SimpleCalculator();
        c2.setFirstNumber(12);
        c2.setSecondNumber(13);
        System.out.println(c2.getAdditionResult());
        System.out.println(c.getSubtractionResult());
        System.out.println(c.getMultiplicationResult());
        System.out.println(c.getDivisionReult());

    }
}
