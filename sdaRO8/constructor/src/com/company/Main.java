package com.company;

public class Main {

    public static void main(String[] args) {
	    car skoda = new car(4);
        car seat = new car(5, 100,"ibiza");
        car audi = new car();
        car bmw = new car(2);
        car vw = new car(5,100_000,"golf");
        System.out.println(skoda.getDoors());
        System.out.println(audi.getDoors());
        System.out.println(seat.getModel());
        System.out.println(bmw.getDoors());
        System.out.println(vw.getKm());
    }
}
