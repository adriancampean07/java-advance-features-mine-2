package com.company;

public class car {
    private int doors;
    private String model;
    private  int km;

    public void setKm(int km) {
        this.km = km;
    }

    public int getKm() {
        return km;
    }

    public car (int doors, int km, String model) {
        this.km = km;
        this.doors = doors;
        this.model = model;
    }

    public car(int doors, String model){
        this.doors = doors;
        this.model = model;
        add();
    }

    private void add(){
        this.doors ++;
    }

    public  car(int doors){
        System.out.println("new car is created");
        this.doors = doors;
    }

    public car(){}


    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
