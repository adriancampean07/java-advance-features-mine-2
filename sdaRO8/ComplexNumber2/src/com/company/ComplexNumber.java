package com.company;

public class ComplexNumber {
    private double real;
    private double imaginary;

    public ComplexNumber(double real, double imaginary){
        this.real = real;
        this.imaginary = imaginary;
    }
    public void add (double real, double imaginary){
        this.real = this.real + real;
        this.imaginary = this.imaginary + imaginary;
    }

    public ComplexNumber add(ComplexNumber c){
       double xr = this.real + c.getReal();
       double xi = this.imaginary + c.getImaginary();
       ComplexNumber newComplexNumber = new ComplexNumber(xr, xi);
       return newComplexNumber;
       }

   public ComplexNumber subtract(ComplexNumber b){
        double yr = this.real - b.getReal();
        double yi = this.imaginary - b.getImaginary();
        ComplexNumber newComplexNumber1 = new ComplexNumber(yr,yi);
        return newComplexNumber1;
        }


    public void subtract (double real, double imaginary){
        this.real = this.real - real;
        this.imaginary = this.imaginary - imaginary;
    }

    public static int add(int a, int b){
        return  a + b;
    }

    public static int multiply(int a,int b){
        return a * b;
    }


    public double getReal() {
        return real;
    }

    public double getImaginary() {
        return imaginary;
    }
}
